FROM node:latest AS builder

RUN mkdir /app

ADD package.json /app

WORKDIR /app

RUN npm install

ADD . /app

RUN npm run build

FROM nginx:stable

ADD nginx.conf /etc/nginx/nginx.conf

COPY --from=builder /app/dist /usr/share/nginx/html
